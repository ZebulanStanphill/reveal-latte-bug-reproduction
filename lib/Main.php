<?php declare(strict_types=1);
namespace Zeb\RevealLatteBugReproduction;

final class Main {
	public static function doStuff(): string {
		$latte = new \Latte\Engine();
		$latte->setTempDirectory(__DIR__ . '/../latte-temp');

		return $latte->renderToString(
			__DIR__ . '/CoolTemplate.latte',
			new CoolTemplate(
				foo: 'bar'
			)
		);
	}
}
