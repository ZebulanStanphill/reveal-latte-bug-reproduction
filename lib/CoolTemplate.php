<?php declare(strict_types=1);
namespace Zeb\RevealLatteBugReproduction;

final class CoolTemplate {
	public function __construct(public string $foo) {}
}
